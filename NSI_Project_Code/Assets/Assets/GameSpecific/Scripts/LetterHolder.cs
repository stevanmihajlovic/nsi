﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Reusable.Scripts;
using TMPro;
using UnityEngine;
using Display = Reusable.Scripts.Display;

namespace GameSpecific.Scripts
{
	public class LetterHolder : MonoBehaviour, IHolder
    {

		public static int[] LETTER_COUNT = new int[30];

		public TextMeshPro Text;
	
		public Display Display;

		private GameController _controller;

        public BoxCollider2D ContainerCollider;

	    private string _remainingCharacters;

        private string _letterToCheck;

        private RandomWordSwapper _randomWordSwapper;

        private int _score;

        // Use this for initialization
        void Awake()
		{
			_score = 0;
		}
 
		public LetterHolder Init(List<string> words, GameController controller)
		{         
			_randomWordSwapper = RandomWordSwapper.Instantiate(words);
			GetWord();
			_score = 0;
				
			Display.SetStyle((int)Display.TEXT_STYLE.LETTER_GAME_STYLE);
			Display.UpdateScore(_score);

			_controller = controller;

			return this;
		}

		private void GetWord()
		{					
			string word = _randomWordSwapper.GetRandomWord();
			_remainingCharacters = GetCharactersOnly(word);
			Text.text = word;

            //Don't forget to call ForceMeshUpdate
            Text.ForceMeshUpdate();

            //has to be same as word for their Indexes to match
            _letterToCheck = word;

            Bounds textBounds = Text.textBounds;

            ContainerCollider.size = textBounds.size;
        }

        private string GetCharactersOnly(string stringToModify)
        {
            stringToModify = stringToModify.Replace(" ", string.Empty);
            stringToModify = stringToModify.Replace("-", string.Empty);
            return stringToModify;
        }

		public void AddPoint()
		{
			_score++;
			if (_remainingCharacters.Length == 0)
			{
				StartCoroutine(UpdateWord());				
			}			
			Display.UpdateScore(_score);
		}

	    public IEnumerator UpdateWord()
	    {
		    yield return new WaitForSeconds(1.75f);

            GetWord();

            if (Text.fontSize < _controller.GetFontSize())
            {
                _controller.SetFontSize(Text.fontSize);
            }
	    }        

        public Vector3 IsObjectOnRightSpot(Collider2D objectCollider, string objectValue)
		{
            if (ContainerCollider.bounds.Intersects(objectCollider.bounds))
            {
				int index = _letterToCheck.IndexOf(objectValue);
				if (index != -1)
				{
					_letterToCheck = _letterToCheck.Insert(index + 1, "!").Remove(index, 1);

					int vertexIndex = Text.textInfo.characterInfo[index].vertexIndex;
					Color32[] colors = Text.textInfo.meshInfo[0].colors32;
					
					colors[vertexIndex + 0].a = 255;
					colors[vertexIndex + 1].a = 255;
					colors[vertexIndex + 2].a = 255;
					colors[vertexIndex + 3].a = 255;

					Text.mesh.colors32 = colors;
					
					_remainingCharacters = _remainingCharacters.Remove(_remainingCharacters.IndexOf(objectValue), 1);		
					//Some delay needed
					AddPoint();
					Vector3 middlePoint = (Text.textInfo.characterInfo[index].bottomLeft + Text.textInfo.characterInfo[index].topRight)/2; 
					
					return Text.transform.TransformPoint(middlePoint);							
				}
				else
				{
					return Vector3.zero;
				}
			}
			else
			{
				return Vector3.zero;
			}
		}

	    public string GetRemainingCharacters()
	    {
		    return _remainingCharacters;
	    }

	    public void SetFontSize(float size)
	    {
		    Text.fontSize = size;
		    Text.fontSizeMax = size;
	    }

	    public float GetFontSize()
	    {
		    return Text.fontSize;
	    }
    }

	public class RandomWordSwapper
	{
		private List<string> _availableWords;

		private static RandomWordSwapper _instance;

		private RandomWordSwapper()
		{
			
		}

		private RandomWordSwapper(List<string> words)
		{
			_availableWords = words;
		}

		public static RandomWordSwapper Instantiate(List<string> words)
		{
			if (_instance == null)
			{
				_instance = new RandomWordSwapper(words);				
			}

			return _instance;
		}

		public string GetRandomWord()
		{
			if (_instance == null)
				return "";
			
			int count = _availableWords.Count;
			if (count > 0)
			{
				
				int random = UnityEngine.Random.Range(0, count - 1);
				string randomWord = _availableWords.ElementAt(random);
				_availableWords.RemoveAt(random);
				return randomWord;
			}
			else
			{
				return "";
			}									
		}

		public static void Destroy()
		{
			if (_instance != null)
			{
				_instance = null;
			}
		}
	}
}


