﻿using System;
using System.Collections;
using System.Collections.Generic;
using Reusable.Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using SVGImporter;

namespace GameSpecific.Scripts
{
    public class ToolCategorySelection : MonoBehaviour
    {
        public GameController GameController;
        public GameMenu GameMenu;
        public CanvasFader ImageFader;
        
        private void Awake()
        {
            CreateCategoryMenu();
        }

        private void SetCurved()
        {
            TextMeshProUGUI[] btns = GetComponentsInChildren<TextMeshProUGUI>();
            foreach (var btn in btns)
            {
                StartCoroutine(btn.GetComponent<TextAlongCircle>().WarpText());
            }
        }

        private void OnEnable()
        {
            SetCurved();
        }

        public void SizeChanged()
        {
            SetCurved();
        }

        /// <summary>
        /// Instantiate a panel on a canvas
        /// Foreach category instantiate a button on a panel
        /// Set name and image of a button according to category
        /// For adding more categories, only config should be changed and a image for that added to Tool
        /// This only happens once, set panel active/inactive on end/start of each level.
        /// In SetGame is the rest of setup based on category
        /// </summary>
        public void CreateCategoryMenu()
        {
            bool rotateClockWise = true;
            List<string> possibleCategories = GameController.GetPossibleCategories();
            foreach (string category in possibleCategories)
            {
                GameObject button = Instantiate(GameController.CategoriesButtonPrefab, GameController.CategoriesMenu.transform);
                button.GetComponent<ButtonRotation_Interval>().SetRotation(rotateClockWise);
                rotateClockWise = !rotateClockWise;
                TextMeshProUGUI[] btns = button.GetComponentsInChildren<TextMeshProUGUI>();
                foreach (var btn in btns)
                {
                    ImageFader.AddText(btn);
                    btn.text = category.Substring(0, 1).ToUpper() + category.Substring(1, category.Length - 1);
                    //btn.GetComponent<ButtonRotation_Interval>().SetRotation(rotateClockWise);
                    StartCoroutine(btn.GetComponent<TextAlongCircle>().WarpText());
                }
                string referenceSaver;// = new string(category);
                referenceSaver = category;
                button.name = referenceSaver;
                button.GetComponent<SVGImage>().vectorGraphics = GameController.RotatingObjectUGUIAssets[possibleCategories.IndexOf(category)];
                button.GetComponent<Button>().onClick.AddListener(() => GameMenu.PlayGame(button.name));
                
                //AddImageForFader
                ImageFader.AddImage(button.GetComponent<SVGImage>());
            }
        }
    }
}
