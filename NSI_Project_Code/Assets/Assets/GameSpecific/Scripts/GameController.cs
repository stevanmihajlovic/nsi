﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using SVGImporter;
using Reusable.Scripts;
using System;
using SimpleJSON;
using TMPro;
using Random = UnityEngine.Random;
using UnityEngine.UI;

namespace GameSpecific.Scripts
{
    public class GameController : MonoBehaviour
    {
        public GameObject CategoriesButtonPrefab;
        public GameObject CategoriesMenu;
        public GameObject LetterHolderPrefab;
        public GameObject CorrectParticles;
        public MultiTouchController MultiTouchController;
        public ToolCategorySelection ToolCategorySelection;
		public SVGFader BackgroundOverlay;
        public TTTSoundManager SoundManager;

        public int TotalLettersOnScene;
        public SVGAsset[] RotatingObjectAssets;
        public SVGAsset[] ContainerAssets;
        public SVGAsset[] RotatingObjectUGUIAssets; //unused 
        public GameObject LetterPrefab;
        public GameObject LeftObject;
        public GameObject RightObject;
        public GameObject Timer;
        public PlayerSelection PlayerSelection;
		public ResizeSVGToScreen[] SVGResizers;
        public GameMenu GameMenu;

        private List<int> _activePlayers;
        private string _chosenCategory;
        private List<string> _possibleWords;     
        private List<string> _possibleCategories;
        private string _possibleLetters;
        private float _time;
        private TextMeshPro _timerText;
        private bool _startTimer;
        private Transform _allLetters;
        private int _gameLength;
        private float _minFontSize;
        private int _minimumMovesPerPlayer;
        private LetterHolder[] _visibleLetterHolders;
        private static JSONNode _lang;
        private static JSONNode _config;
		private Vector2 _currentScreenRes;
        private bool _isSoundMute = false;

        public static void SetConfig(JSONNode lang, JSONNode config)
        {
            _lang = lang;
            _config = config;
        }
        
        
        public Rect ToolArea
        {
            get;
            private set;
        }
        
        void Awake()
        {
			MultiTouchController = GetComponent<MultiTouchController> ();

            _minFontSize = 100000;
            _visibleLetterHolders = new LetterHolder[4];
            _minimumMovesPerPlayer = 3;
            _startTimer = false;
            _gameLength = 120;
            //_possibleLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            _possibleLetters = "ABCDEFGHIJKLMNOPRSTUVZ";

            ToolArea = new Rect(0, 0, 2 * Screen.width, 2 * Screen.height);
            //Load config
            LoadCategories();

            float height = Camera.main.orthographicSize;
            float width = height * Camera.main.aspect;
            
            ToolArea = new Rect(0,0,2*width, 2*height); 
			_currentScreenRes = new Vector2 (Screen.width, Screen.height);
        }

        private void Update()
        {
            if (_startTimer)
            {
                _time += Time.deltaTime;
                int timeRemaining = (_gameLength - (int) _time);
                _timerText.text = timeRemaining.ToString();
                if (timeRemaining == 0)
                    EndSession();
            }

			if (Screen.width != _currentScreenRes.x || Screen.height != _currentScreenRes.y)
			{
				_currentScreenRes = new Vector2 (Screen.width, Screen.height);

				foreach (ResizeSVGToScreen resizer in SVGResizers)
				{
					resizer.Resize ();
				}

				if (_activePlayers != null)
				{
					StartCoroutine (SetContainersPositions ());
				}

                ToolCategorySelection.SizeChanged();

            }
        }

        public void SetPlayers()
        {
            _activePlayers = new List<int>();
            bool[] activePlayers = PlayerSelection.GetActivePlayers();
            for (int i = 0; i < activePlayers.Length; i++)
                if (activePlayers[i])
                    _activePlayers.Add(i);
        }

        public void SetGame(string category)
        {
            SoundManager.PlayOneShot("Button");

            if (_allLetters == null)
            {
                GameObject newGO = new GameObject("Letters");
                newGO.transform.parent = this.transform.parent;
                _allLetters = newGO.transform;
            }

            _chosenCategory = category;         

            _startTimer = true;
            _time = 0;
            Timer.SetActive(true);
            _timerText = Timer.GetComponentInChildren<TextMeshPro>();
            _timerText.text = _gameLength.ToString();

            //load Words. It is considered that proper language json file is lang.json
            LoadWords();

            //Set proper images depending on _category and Set visible objects
            InstantiateContainers();
            
            LeftObject.GetComponent<SVGRenderer>().vectorGraphics = RotatingObjectAssets[2 * _possibleCategories.IndexOf(_chosenCategory)];
            RightObject.GetComponent<SVGRenderer>().vectorGraphics = RotatingObjectAssets[2 * _possibleCategories.IndexOf(_chosenCategory) + 1];
            LeftObject.SetActive(true);
            RightObject.SetActive(true);
                        
			BackgroundOverlay.Fade (false);

			MultiTouchController.enabled = true;

            SetLevel();
        }

        private void SetLevel()
        {
            int ballCount = 0;
            foreach (var player in _activePlayers)
            {
                string remainingCharacters = _visibleLetterHolders[player].GetRemainingCharacters();
                var shuffled = remainingCharacters.OrderBy(n => UnityEngine.Random.Range(0, remainingCharacters.Length - 1)).ToList();
                for (int j = 0; j < _minimumMovesPerPlayer; j++)
                {
                    InstantiateLetter(shuffled[j].ToString(), player, true);
                    ballCount++;
                }
            }

            while (ballCount++ < TotalLettersOnScene)
            {
                InstantiateLetter(_possibleLetters[Random.Range(0, _possibleLetters.Length - 1)].ToString(), -1, true);
            }

            SoundManager.PlaySound(_chosenCategory);
        }
        //TODO: Make this static!
		//TODO Revert this after testing
        /*JSONNode FindObjectsByValueOfKey(JSONNode parent,string parentKey, string key, string value)
        {

            JSONArray array = parent[parentKey] as JSONArray;

            JSONNode result = array.Children.Where(x => (x[key] == value)).ToList().FirstOrDefault();

            return result;
        }*/

        public void LoadWords()
        {
			//TODO Revert this after testing
			/*
            JSONNode objectsOfCertainCategory = FindObjectsByValueOfKey(_lang, "categories", "name", _chosenCategory);
            JSONArray ja =  objectsOfCertainCategory["words"].AsArray;
            _possibleWords = ja.Children.Select(x => x.Value.ToString()).ToArray().ToList();*/

			switch (_chosenCategory)
			{
			case "povrce":
				_possibleWords = new List<string> {"PARADAJZ", "TIKVA", "LUBENICA", "PASULJ", "KRASTAVAC", "KUPUS", "KROMPIR", "CELER", "KARFIOL",
                        "CRNI LUK", "BELI LUK", "BROKOLI", "KELJ", "ZELENA SALATA", "BORANIJA", "BIBER", "PAPRIKA", "ROTKVICA",
                        "BUNDEVA", "DINJA"
                };
				break;
			case "sport":
				_possibleWords = new List<string> {"AEROBIK", "BADMINTON", "BILIJAR", "BOKS", "KUGLANJE", "KRIKET", "KARLING", "PIKADO", "DIZANJE TEGOVA", "GOLF",
                        "RUKOMET", "HOKEJ", "BEJZBOL", "KARATE", "KARTING", "KIK BOKS", "KLIZANJE", "SKIJANJE", "POLO", "MOTOKROS",
                        "VATERPOLO", "BICIKLIZAM", "PING PONG", "TENIS", "SKIJANJE", "BRZO HODANJE", "SNUKER", "FUDBAL", "TRIATLON",
                        "ODBOJKA", "PLIVANJE"
                };
				break;
			case "gradovi":
				_possibleWords = new List<string> {"AMSTERDAM", "ATINA", "BRATISLAVA", "BANGKOK", "BARSELONA", "BERLIN", "BEOGRAD", "BRISEL",
                        "CIRIH", "DUBAI", "DABLIN", "FRANKFURT", "HONG KONG", "ISTANBUL", "KOPENHAGEN", "LISABON",
                        "LONDON", "MADRID", "MELBURN", "MEKSIKO SITI", "MILAN", "MOSKVA", "MINHEN", "PARIZ", "PEKING", "PRAG",
                        "ROTERDAM", "RIM", "STOKHOLM", "SIDNEJ", "TOKIO", "VANKUVER"
                };
				break;
			}
        }

        public void LoadCategories()
        {
			//TODO Revert this after testing
			/*
            JSONArray jsonArray = _config["categories"].AsArray;
            _possibleCategories = jsonArray.Children.Select(x => x.Value.ToString()).ToArray().ToList();*/
			_possibleCategories = new List<string> { "povrce", "sport", "gradovi" };
        }              

        public void InstantiateLetter(string value, int forWhichPlayer, bool initialSpawn)
        {
            Vector3 randomPosition = new Vector3(UnityEngine.Random.Range(-ToolArea.width / 2, ToolArea.width / 2), UnityEngine.Random.Range(-ToolArea.height / 2, ToolArea.height / 2), 0);
            GameObject go = Instantiate(LetterPrefab,randomPosition,Quaternion.Euler(0, 0, UnityEngine.Random.Range(-1f, 1f)), _allLetters.transform) as GameObject;
            go.GetComponent<MovableObject>().Init(value, this.IsObjectOnCorrectPlace, forWhichPlayer, null, SpawnNewLetter, initialSpawn);
        }

        public void IsObjectOnCorrectPlace(Collider2D objectCollider, string objectValue, MovableObject letter)
        {
			Destroy(letter.Dragging);

            foreach (var player in _activePlayers)
            {
                Vector3 vector = _visibleLetterHolders[player].IsObjectOnRightSpot(objectCollider, objectValue);
                if (vector != Vector3.zero)
                {
                    GameObject particles = Instantiate(CorrectParticles, new Vector3(letter.transform.position.x,letter.transform.position.y, -1), Quaternion.identity);
                    particles.SetActive(true);
                    Destroy(particles, 1.5f);

                    SoundManager.PlayOneShot("Click");
                    
                    SpawnNewLetter(letter.ForWhichPlayer(), false, 0);

                    letter.SetToSpot(vector, _visibleLetterHolders[player].gameObject.transform.rotation.eulerAngles.z,
                        new Vector3(1.136f, 1.136f, 1.136f), _visibleLetterHolders[player].GetFontSize());
                    return;
                }
            }
            TextMeshPro tmp = letter.GetComponent<TextMeshPro>();
            if (tmp != null)
            {
                Color c = tmp.color;
                c.a = 0.5f;
                tmp.color = c;
            }

            letter.AnimatorComponent.enabled = true;
            Destroy(letter.gameObject, 1.1f);

            SpawnNewLetter(letter.ForWhichPlayer(), false, 0);
        }
        
        public void EndSession()
        {
            SoundManager.StopSounds();
            SoundManager.PlayOneShot("Ready");

			MultiTouchController.enabled = false;
            _startTimer = false;
            _allLetters.BroadcastMessage ("FadeOut", SendMessageOptions.DontRequireReceiver);

            GameMenu.ShowEndGamePanel();
        }

        public void RestartGame()
        {
            SoundManager.StopSounds();

			BackgroundOverlay.Fade (true);
            GameMenu.HideEndGamePanel();
			
            //RandomWordSwapper.Destroy();
            
            Invoke("BackToCategorySelection", 1.3f);
        }

        private void BackToCategorySelection()
        {
            LeftObject.SetActive(false);
            RightObject.SetActive(false);
            Timer.SetActive(false);
            GameMenu.GoToCategoriesChooser();
        }        

        private void InstantiateContainers()
        {
            Vector2[] array = new Vector2[]{new Vector2(0.5f,0), new Vector2(1, 0.5f) ,new Vector2(0.5f,1), new Vector2(0,0.5f), };

            foreach (var player in _activePlayers)
            {
                Vector3 pos = Camera.main.ViewportToWorldPoint(array[player]);
                pos.z = 0;
                GameObject holder = Instantiate(LetterHolderPrefab, pos, Quaternion.Euler(new Vector3(0, 0, player * 90)), this.transform);
                holder.SetActive(true);
                holder.GetComponentInChildren<SVGRenderer>().vectorGraphics = ContainerAssets[_possibleCategories.IndexOf(_chosenCategory)];                              
                _visibleLetterHolders[player] = holder.GetComponent<LetterHolder>().Init(_possibleWords, this);
                float fontSize = _visibleLetterHolders[player].GetFontSize();

                if (_minFontSize > fontSize)
                {
                    _minFontSize = fontSize;
                }
            }
            
            SetFontSize(_minFontSize);
        }

		private IEnumerator SetContainersPositions()
		{
			yield return new WaitForEndOfFrame();

			Vector2[] array = new Vector2[]{new Vector2(0.5f,0), new Vector2(1, 0.5f) ,new Vector2(0.5f,1), new Vector2(0,0.5f), };

			foreach (var player in _activePlayers)
			{
				Vector3 pos = Camera.main.ViewportToWorldPoint(array[player]);
				pos.z = 0;

				_visibleLetterHolders [player].transform.position = pos;
			}

		}

		public void SpawnNewLetter(int forWhichPlayer, bool initialSpawn, float zPosition)
        {
            if (forWhichPlayer != -1)
            {
                string remainingCharacters = _visibleLetterHolders[forWhichPlayer].GetRemainingCharacters();

                if (remainingCharacters.Length != 0)
                {
                    InstantiateLetter(remainingCharacters[UnityEngine.Random.Range(0, remainingCharacters.Length - 1)].ToString(), forWhichPlayer, initialSpawn);
                }
            }
            else
            {
                InstantiateLetter(_possibleLetters[Random.Range(0, _possibleLetters.Length - 1)].ToString(), -1, initialSpawn);
            }
        
        }

        public void SetFontSize(float minFontSize)
        {
            _minFontSize = minFontSize;
            foreach (var player in _activePlayers)
            {
                _visibleLetterHolders[player].SetFontSize(_minFontSize);
            }
        }

        public float GetFontSize()
        {
            return _minFontSize;
        }

        public void MuteSounds()
        {
            _isSoundMute = !_isSoundMute;
            SoundManager.Mute(_isSoundMute);
        }

        public void FadeAllLetters()
        {
            if (_allLetters != null)
            {
                Destroy(_allLetters.gameObject);
                foreach (var player in _activePlayers)
                {
                    Destroy(_visibleLetterHolders[player].gameObject);
                }
            }                
        }

        public List<string> GetPossibleCategories()
        {
            return _possibleCategories;
        }
    }
}
