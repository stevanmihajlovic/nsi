﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Reusable.Scripts;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using SVGImporter;
using System;

namespace GameSpecific.Scripts
{
    public class GameMenu : MonoBehaviour
    {
        public GameObject EndGamePanel;
        public GameObject Canvas;

        public PlayerSelection PlayerSelection;
        public ToolCategorySelection ToolCategorySelection;
        public GameController GameController;       
        public Button PlayButton;

        public GameObject TopButton;
        public GameObject BottomButton;

        private int _viewNumber;
        
        public SVGFader BackgroundOverlay;
        
        void Awake()
        {
            GoToPlayerChooser();
            _viewNumber = 1;
        }


        public void GoToPlayerChooser()
        {
            ShowPlayerSelectionMenu();
            HideCategoriesSelectionMenu();
            HideEndGamePanel();
        }

        public void GoToCategoriesChooser()
        {
            _viewNumber = 2;
            GameController.SetPlayers();
            RandomWordSwapper.Destroy();
            SetBackButtonVisible(true);
            GameController.FadeAllLetters();
            HidePlayerSelectionMenu();
            ShowCategoriesSelectionMenu();
            ToolCategorySelection.ImageFader.Unfade();
        }

        public void PlayGame(string category)
        {
            _viewNumber = 3;
            GameController.SetGame(category);
            
            HidePlayerSelectionMenu();
            HideCategoriesSelectionMenu();
        }

        public void ShowEndGamePanel()
        {
            EndGamePanel.SetActive(true);
        }

        public void HideEndGamePanel()
        {
            EndGamePanel.SetActive(false);
        }

        public void ShowPlayerSelectionMenu()
        {
            PlayerSelection.gameObject.SetActive(true);
        }

        public void HidePlayerSelectionMenu()
        {
            PlayerSelection.gameObject.SetActive(false);
        }

        public void ShowCategoriesSelectionMenu()
        {
            ToolCategorySelection.gameObject.SetActive(true);
        }

        public void HideCategoriesSelectionMenu()
        {
            ToolCategorySelection.gameObject.SetActive(false);
        }

        public void SetListenerForPlayButton(UnityAction listener)
        {
            PlayButton.onClick.RemoveAllListeners();
            PlayButton.onClick.AddListener(listener);
        }
        
        public void OnBackButton()
        {
            if (_viewNumber == 3)
            {
                BackgroundOverlay.Fade(true);
                GameController.SoundManager.StopSounds();
                Invoke("GoToCategoriesChooser", 1.3f);
                _viewNumber = 2;
            }
            else if (_viewNumber == 2)
            {
                ToolCategorySelection.ImageFader.Fade();

                SetBackButtonVisible(false);
                Invoke("GoToPlayerChooser", 1.3f);
                _viewNumber = 2;
            }
        }

        private void SetBackButtonVisible(bool visible)
        {
            TopButton.SetActive(visible);
            BottomButton.SetActive(visible);
        }
    }
}
