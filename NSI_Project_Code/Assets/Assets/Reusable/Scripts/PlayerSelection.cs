﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using SVGImporter;

namespace Reusable.Scripts
{
	public class PlayerSelection : MonoBehaviour
	{
        public TextMeshProUGUI Text;
        public GameObject PlayButtonImage;

		public GameObject PlayButton;
		private Toggle[] _playerToggles;


		void Awake ()
		{
			PlayButton.SetActive (false);
			_playerToggles = GetComponentsInChildren<Toggle> (true);

			foreach (Toggle tgl in _playerToggles)
				tgl.isOn = false;

		}

        public void Init(bool useStartButtonText, string startButtonText)
        {
            if (useStartButtonText)
            {
                Text.text = startButtonText;
            }
            else
            {
                PlayButtonImage.SetActive(true);
            }
        }

        public void SwitchToggle(bool isOn)
		{
			// TODO: I've changed this method to use the Unity Toggle component to swap the colors, but that component doesn't automatically keep the Toggle in the Active Color and the code below forces that.
			int amountOfActiveToggles = 0;
			foreach (Toggle toggle in _playerToggles)
			{
				if (toggle.isOn)
				{
					setButtonColors(toggle, Colors.DefaultButtonActive, Colors.DefaultButtonActive);
					amountOfActiveToggles++;
				}
				else
				{
					setButtonColors(toggle, Colors.DefaultButtonIdle, Colors.DefaultButtonHover);
				}
			}

			if (amountOfActiveToggles > 0)
			{
				PlayButton.SetActive(true);
			}
			else
			{
				PlayButton.SetActive(false);
			}
		}

		public bool[] GetActivePlayers()
		{
			bool[] activePlayers = new bool[4];

			for (int i = 0; i < 4; i++)
				activePlayers [i] = _playerToggles [i].isOn;

			return activePlayers;
		}

		private void setButtonColors(Toggle toggle, Color idle, Color hover)
		{
			ColorBlock cBlock = toggle.colors;
			cBlock.normalColor = idle;
			cBlock.highlightedColor = hover;
			toggle.colors = cBlock;
		}
	}
}
