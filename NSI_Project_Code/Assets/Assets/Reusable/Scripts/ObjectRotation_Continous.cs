﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reusable.Scripts
{
    public class ObjectRotation_Continous : MonoBehaviour
    {
        private Transform _transform;

        public bool Clockwise = true;

        public float RotateDegrees = 360f;
        private float _rotateDegrees;

        public float RotateTime = 16f;
        private float _rotateTime;

        void Awake()
        {
            _transform = GetComponent<Transform>();

            _rotateDegrees = RotateDegrees;
            if (Clockwise) _rotateDegrees = -RotateDegrees;

            _rotateTime = RotateTime;

            rotate();
        }

        private void rotate()
        {
            LeanTween.rotateAround(gameObject, Vector3.forward, _rotateDegrees, _rotateTime)
    .setEase(LeanTweenType.easeInOutQuad).setLoopType(LeanTweenType.linear);
        }

        private void OnEnable()
        {
            LeanTween.resume(gameObject);
        }

        private void OnDisable()
        {
            LeanTween.pause(gameObject);
        }

    }
}



