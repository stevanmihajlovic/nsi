﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

namespace Reusable.Scripts
{
	public class Display : MonoBehaviour
	{
		public enum TEXT_STYLE {LETTER_GAME_STYLE, CAR_GAME_STYLE, BALL_GAME_STYLE};

		private int _style;
		
		private int _score;

		private TextMeshPro _tmp;

		void Awake()
		{			
			_score = 0;
			_tmp = GetComponentInChildren<TextMeshPro>();
			_tmp.text = GetFormatedScore();
		}

		public void SetStyle(int style)
		{
			if (style < 3)
				_style = style;
			else
			{
				_score = 0;
			}
		}

		private string GetFormatedScore()
		{
			string score = "";
			switch (_style)
			{
					case (int)TEXT_STYLE.CAR_GAME_STYLE:
						score = "0" + _score.ToString();//to do format!
						break;
					case (int)TEXT_STYLE.BALL_GAME_STYLE:
						score = "<color=#333333>00</color><color=#fa0000>" + _score.ToString() +"</color>";
						break;
					case (int)TEXT_STYLE.LETTER_GAME_STYLE:
					default:	
						score = _score.ToString();
						break;					
			}

			return score;
		}
		
		public void UpdateScore(int score)
		{
			_score = score;
			_tmp.text = GetFormatedScore();
		}
	}
}


