﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reusable.Scripts
{
    public class RotatableObject : MonoBehaviour
    {

        public float RotateSpeed;

        public float RotateDirection;

        // Update is called once per frame
        void Update()
        {

            transform.Rotate(new Vector3(0, 0, RotateDirection) * Time.deltaTime * RotateSpeed);

        }
    }
}
