﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reusable.Scripts
{
	public class Fader : MonoBehaviour
	{
		private SpriteRenderer _renderer;
		private Color _fadeColor;
		private bool _fade;
		private float _fadeSpeed;

		public delegate void FadeOverEventHandler(object source, EventArgs e);
		public event FadeOverEventHandler OnFadeOver;

		void Awake()
		{
			_fade = false;
			_renderer = GetComponent<SpriteRenderer> ();
			_renderer.sortingOrder = 101;
			_fadeSpeed = 1;
		}

		public void Fade(bool fadeIn)
		{
			if (fadeIn)
			{
				_renderer.color = new Color (1, 1, 1, 0);
				_fadeColor = Color.white;
				_fadeSpeed = 3f;
			} else
			{
				_renderer.color = Color.white;
				_fadeColor = new Color (1, 1, 1, 0);
				_fadeSpeed = 2f;
			}
			_fade = true;
		}

		void Update()
		{
			if (_fade) 
			{
				_renderer.color = Color.Lerp (_renderer.color, _fadeColor, _fadeSpeed * Time.deltaTime);
				if (Mathf.Abs (_renderer.color.a - _fadeColor.a) <= 0.01)
				{
					_renderer.color = _fadeColor;
					_fade = false;

					if (OnFadeOver != null)
					{
						OnFadeOver(this, new EventArgs());
					}
				}
			}
		}
	}
}
