﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reusable.Scripts
{
    public class Colors : MonoBehaviour
    {
        //public static Color DefaultButtonIdle = new Color32(0x22, 0x66, 0xAF, 0xFF);
        //public static Color DefaultButtonHover = new Color32(0x59, 0x8C, 0xC3, 0xFF);
        //public static Color DefaultButtonActive = new Color32(0xFC, 0xC3, 0x00, 0xFF);
        public static Color DefaultButtonIdle = new Color32(0xEC, 0x97, 0x1F, 0xFF);
        public static Color DefaultButtonHover = new Color32(0xD5, 0x85, 0x12, 0xFF);
        public static Color DefaultButtonActive = new Color32(0x33, 0x7A, 0xB7, 0xFF);
    }
}
