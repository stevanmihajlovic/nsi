﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reusable.Scripts
{
    public interface IDrag
    {
        List<int> TouchIds { get; set; } //added this entire property
        void RegisterTouch(int touchId, Vector2 newPosition);
        void MoveTo(int touchId, Vector2 newPosition); //Added one parameter
        void UnregisterTouch(int touchId, Vector2 newPosition); //Added one Parameter
    }
}

