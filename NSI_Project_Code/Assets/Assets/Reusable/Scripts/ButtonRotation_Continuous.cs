﻿using UnityEngine;

namespace Reusable.Scripts
{
    public class ButtonRotation_Continuous : MonoBehaviour
    {
        private RectTransform _rectTransform;

        public bool Clockwise = true;

        public float RotateDegrees = 360f;
        private float _rotateDegrees;

        public float RotateTime = 16f;
        private float _rotateTime;

        void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();

            _rotateDegrees = RotateDegrees;
            if (Clockwise) _rotateDegrees = -RotateDegrees;

            _rotateTime = RotateTime;

            rotate();
        }

        private void rotate()
        {
            LeanTween.rotate(_rectTransform, _rotateDegrees, _rotateTime)
                .setLoopType(LeanTweenType.linear);
        }

        private void OnEnable()
        {
            LeanTween.resume(gameObject);
        }

        private void OnDisable()
        {
            LeanTween.pause(gameObject);
        }

        public void SetRotation(bool clockwise)
        {
            LeanTween.pause(gameObject);
            Clockwise = clockwise;
            if (clockwise)
            {
                _rotateDegrees = -RotateDegrees;
            }
            else
            {
                _rotateDegrees = RotateDegrees;
            }

            rotate();
        }
    }
}