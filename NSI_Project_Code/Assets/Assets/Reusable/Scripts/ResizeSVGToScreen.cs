﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SVGImporter;

namespace Reusable.Scripts
{
	public class ResizeSVGToScreen : MonoBehaviour
	{
		public SVGRenderer Renderer;

		void Awake ()
		{
			Resize ();
        }

		public void Resize()
		{
			transform.localScale = new Vector3(1, 1, 1);

			float width = Renderer.vectorGraphics.bounds.size.x;
			float height = Renderer.vectorGraphics.bounds.size.y;

			float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
			float worldScreenWidth = worldScreenHeight * Screen.width / Screen.height;

			float newScaleAxis = Mathf.Max (worldScreenWidth / width, worldScreenHeight / height);

			transform.localScale = new Vector3 (newScaleAxis, newScaleAxis, 1);	
		}
    }
}
