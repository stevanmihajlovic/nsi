﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
/// <summary>
/// This is component written in order to ease our pain with wrapping text around circles on canvas. We attach this script to gameobject that has textmeshpro 
/// component attached. It can wrap word around circle if word is placed above or below circle. As needed attributes, we need to give reference to game object
/// (with circle image) around which we want to wrap, and relative position of text in respect to that circle game object. It's not important that bounds of 
/// text touch border of circle (you can make a gap), but it is very important that you align their centers vertically. Also this algorithm uses distances between 
/// those two objects in canvas space, so if canvas has some scale factor - it needs to have acces to that. That's why it's important that you assign the name 
/// of game object that has canvas attached to it!
/// </summary>
namespace Reusable.Scripts
{
    public class TextAlongCircle : MonoBehaviour
    {

        private TMP_Text m_TextComponent;
        private float _curveScale = 1.0f;
        private Canvas _canvas;
        private GameObject _canvasGameObject;

        private float _textWidth;
        private float _textHeight;
        private Vector3 _circlePosition;
        private Vector3 _textPosition;
        private float _arcRadius;

        public AnimationCurve VertexCurve;
        public GameObject WarpAroundThis;

        public string NameOfCanvasObject = "Canvas";
        public bool TextBelowCircleCenter = true;

        void Awake()
        {
            m_TextComponent = gameObject.GetComponent<TMP_Text>();
            GetCanvas();
        }

        private AnimationCurve CopyAnimationCurve(AnimationCurve curve)
        {
            AnimationCurve newCurve = new AnimationCurve();
            newCurve.keys = curve.keys;
            return newCurve;
        }

        private void GetCanvas()
        {
            _canvasGameObject = GameObject.Find(NameOfCanvasObject);
            if (_canvasGameObject != null)
            {
                _canvas = _canvasGameObject.GetComponent<Canvas>();
            }
        }
        private Vector3 GetCanvasScale()
        {
            if (_canvasGameObject != null)
            {
                return _canvasGameObject.GetComponent<RectTransform>().localScale;
            }
            return Vector3.zero;
        }

        private float GetDistanceFromCenterOfCircleToText(Vector3 circlePosition, Vector3 textPosition)
        {
            Vector3 canvasScale = GetCanvasScale();

            Vector3 originalPositionCircle = new Vector3(circlePosition.x / canvasScale.x, circlePosition.y / canvasScale.y, 1);
            Vector3 originalPositionText = new Vector3(textPosition.x / canvasScale.x, textPosition.y / canvasScale.y, 1);

            Vector3 distance = originalPositionCircle - originalPositionText;

            return distance.magnitude;
        }

        private float GetCurveArcRadius(Vector3 circlePosition, Vector3 textPosition, float textHeight)
        {
            float arcRadius = 0.0f;

            if (TextBelowCircleCenter)
            {
                //float arcRadius = textInsideCircle? circleRadius: circleRadius + textHeight; //Old approach, it assumed that text bounds are touching circle
                //Text is below circle center
                arcRadius = GetDistanceFromCenterOfCircleToText(_circlePosition, _textPosition) + _textHeight / 2;  
            }
            else
            {
                //Text is above circle center
                arcRadius = GetDistanceFromCenterOfCircleToText(_circlePosition, _textPosition) - _textHeight / 2;
            }

            return arcRadius;
        }

        private void GetDimensionsAndPositions(GameObject circle)
        {
            _textWidth = m_TextComponent.bounds.size.x; //Bounds of text will always have same size, and they do not depend on orientation(Unlike for colliders for example)!
            _textHeight = m_TextComponent.bounds.size.y;

            RectTransform rt = circle.GetComponent<RectTransform>();
            _circlePosition = rt.position; //circle position in world space
            _textPosition = m_TextComponent.rectTransform.position; //text position in world space

            //Execute this last, since it needs all previus dimensions
            _arcRadius = GetCurveArcRadius(_circlePosition, _textPosition, _textHeight);
        }

        //This method generates convex or concave animation curve around text will warp. It calculates arc around which it should wrap, and distance
        //of bottom of text to that curve. When text is below circle center, bottom of convex curve touches bottom of text bounds, and their vertical axis are aligned
        //When text is above circle center, top of concave curve touches bottom of text bounds(entire curve is below time axis of animation curve). Time axis of animation
        //curve is always between 0-1, and values from 0-1 are being interpolated to text bounds width!
        //How angles are calculated? Imagine that circle is clock with two equal sized clock hands. Vertically they are aligned(center of clock with center of text)
        //One clock hand1 is at 9h, and hand2 at 12h. So initially at 90 degrees. In each iteration hand1 goes towards hand2, and we use sin and cos to calculate the curve.
        private List<Keyframe> CreateArcCurveAlongCircle(GameObject circle)
        {
            GetDimensionsAndPositions(circle);

            List<float> leftPartOfArcValues = new List<float>();
            List<float> rightPartOfArcValues = new List<float>();

            for (int i = 90; i >= 0; i--) //90 degrees
            {
                float angleInRadians = i * Mathf.PI / 180.0f;
                float deltaX = Mathf.Sin(angleInRadians) * _arcRadius;
                float deltaY = Mathf.Cos(angleInRadians) * _arcRadius;

                if (deltaX < _textWidth / 2) //we take the part of arc that is strictly above text (cut off the parts that go too much left and right)
                {
                    if (TextBelowCircleCenter) //we build convex curve whose bottom matches bottom of text
                    {
                        leftPartOfArcValues.Add(_arcRadius - deltaY);
                        rightPartOfArcValues.Insert(0, _arcRadius - deltaY);
                    }
                    else //we build concave curve whose top matches bottom of text
                    {
                        float yValue = -(_arcRadius - deltaY);

                        leftPartOfArcValues.Add(yValue);
                        rightPartOfArcValues.Insert(0, yValue);
                    }
                }
            }

            List<float> valueList = leftPartOfArcValues.Concat(rightPartOfArcValues).ToList();


            //Now, those for those values we generated (y values of vertex curve, we need to generate x values from 0-1, 
            //since x needs to go in that range - it's being interpolated to width of the text later in the algorithm)

            List<Keyframe> keyFrames = new List<Keyframe>();
            for (int j = 0; j < valueList.Count; j++)
            {
                keyFrames.Add(new Keyframe((float)j / valueList.Count, valueList[j]));
            }

            return keyFrames;
        }

        private AnimationCurve CreateAnimationCurve()
        {
            Keyframe[] lkf = CreateArcCurveAlongCircle(WarpAroundThis).ToArray();
            AnimationCurve aCurve = new AnimationCurve(lkf);

            for (int i = 0; i < aCurve.keys.Length; i++)
            {
                aCurve.SmoothTangents(i, 0);
            }
            return aCurve;
        }

        /// <summary>
        ///  Method to curve text along a Unity animation curve.
        /// </summary>
        /// <param name="textComponent"></param>
        /// <returns></returns>
        public IEnumerator WarpText()
        {
            yield return new WaitForEndOfFrame();

            VertexCurve = CreateAnimationCurve();

            VertexCurve.preWrapMode = WrapMode.Clamp;
            VertexCurve.postWrapMode = WrapMode.Clamp;

            Vector3[] vertices;
            Matrix4x4 matrix;

            m_TextComponent.havePropertiesChanged = true; // Need to force the TextMeshPro Object to be updated.

            float old_CurveScale = _curveScale;

            _curveScale = 1; //ALWAYS 1 FOR THIS ALGORITHM
            AnimationCurve old_curve = CopyAnimationCurve(VertexCurve);

            if (!m_TextComponent.havePropertiesChanged && old_CurveScale == _curveScale && old_curve.keys[1].value == VertexCurve.keys[1].value)
            {
                yield return null;
            }


            old_CurveScale = _curveScale;
            old_curve = CopyAnimationCurve(VertexCurve);

            m_TextComponent.ForceMeshUpdate(); // Generate the mesh and populate the textInfo with data we can use and manipulate.

            TMP_TextInfo textInfo = m_TextComponent.textInfo;
            int characterCount = textInfo.characterCount;


            if (characterCount == 0) yield return null;

            float boundsMinX = m_TextComponent.bounds.min.x;  
            float boundsMaxX = m_TextComponent.bounds.max.x;  



            for (int i = 0; i < characterCount; i++)
            {
                if (!textInfo.characterInfo[i].isVisible)
                    continue;

                int vertexIndex = textInfo.characterInfo[i].vertexIndex;

                // Get the index of the mesh used by this character.
                int materialIndex = textInfo.characterInfo[i].materialReferenceIndex;

                vertices = textInfo.meshInfo[materialIndex].vertices;

                // Compute the baseline mid point for each character
                Vector3 offsetToMidBaseline = new Vector2((vertices[vertexIndex + 0].x + vertices[vertexIndex + 2].x) / 2, textInfo.characterInfo[i].baseLine);
                //float offsetY = VertexCurve.Evaluate((float)i / characterCount + loopCount / 50f); // Random.Range(-0.25f, 0.25f);

                // Apply offset to adjust our pivot point.
                vertices[vertexIndex + 0] += -offsetToMidBaseline;
                vertices[vertexIndex + 1] += -offsetToMidBaseline;
                vertices[vertexIndex + 2] += -offsetToMidBaseline;
                vertices[vertexIndex + 3] += -offsetToMidBaseline;

                // Compute the angle of rotation for each character based on the animation curve
                float x0 = (offsetToMidBaseline.x - boundsMinX) / (boundsMaxX - boundsMinX); // Character's position relative to the bounds of the mesh.
                float x1 = x0 + 0.0001f;
                float y0 = VertexCurve.Evaluate(x0) * _curveScale;
                float y1 = VertexCurve.Evaluate(x1) * _curveScale;

                Vector3 horizontal = new Vector3(1, 0, 0);
                //Vector3 normal = new Vector3(-(y1 - y0), (x1 * (boundsMaxX - boundsMinX) + boundsMinX) - offsetToMidBaseline.x, 0);
                Vector3 tangent = new Vector3(x1 * (boundsMaxX - boundsMinX) + boundsMinX, y1) - new Vector3(offsetToMidBaseline.x, y0);

                float dot = Mathf.Acos(Vector3.Dot(horizontal, tangent.normalized)) * 57.2957795f;
                Vector3 cross = Vector3.Cross(horizontal, tangent);
                float angle = cross.z > 0 ? dot : 360 - dot;

                matrix = Matrix4x4.TRS(new Vector3(0, y0, 0), Quaternion.Euler(0, 0, angle), Vector3.one);

                vertices[vertexIndex + 0] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 0]);
                vertices[vertexIndex + 1] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 1]);
                vertices[vertexIndex + 2] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 2]);
                vertices[vertexIndex + 3] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 3]);

                vertices[vertexIndex + 0] += offsetToMidBaseline;
                vertices[vertexIndex + 1] += offsetToMidBaseline;
                vertices[vertexIndex + 2] += offsetToMidBaseline;
                vertices[vertexIndex + 3] += offsetToMidBaseline;
            }


            // Upload the mesh with the revised information
            m_TextComponent.UpdateVertexData();

            yield return new WaitForSeconds(0.025f);
        }
    }
}

