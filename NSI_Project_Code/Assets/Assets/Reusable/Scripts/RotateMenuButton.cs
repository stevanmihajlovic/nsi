﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reusable.Scripts
{
    public class RotateMenuButton : MonoBehaviour
    {
        public float RotateSpeed = 100;

        public float RotateDirection = -1;

        RectTransform _rect;

        void Awake()
        {
            _rect = GetComponent<RectTransform>();
        }

        void Update()
        {
            _rect.Rotate(new Vector3(0, 0, RotateDirection) * RotateSpeed * Time.deltaTime);
        }
    }
}
