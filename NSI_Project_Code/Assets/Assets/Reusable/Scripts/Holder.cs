﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reusable.Scripts
{
    public interface IHolder 
    {
        void AddPoint();
        
        Vector3 IsObjectOnRightSpot(Collider2D objectCollider, string objectValue);
    }
}


