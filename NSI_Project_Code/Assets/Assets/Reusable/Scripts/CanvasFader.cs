﻿using SVGImporter;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Reusable.Scripts
{
    public class CanvasFader : MonoBehaviour
    {
        private Color _fadeColor;
        private bool _fade;
        private float _fadeSpeed;

        public delegate void FadeOverEventHandler(object source, EventArgs e);
        public event FadeOverEventHandler OnFadeOver;

        public List<TextMeshProUGUI> TextsToFade = new List<TextMeshProUGUI>();
        public List<SVGImage> ImagesToFade = new List<SVGImage>();

        void Awake()
        {
            _fade = false;
            //_renderer = GetComponent<SVGImage>();
            _fadeSpeed = 1;
        }

        public void Fade()
        {
            _fadeSpeed = 4f;
            _fade = true;
        }

        public void AddImage(SVGImage image)
        {
            ImagesToFade.Add(image);
        }

        public void AddText(TextMeshProUGUI text)
        {
            TextsToFade.Add(text);
        }

        public void Unfade()
        {
            foreach (var imageToFade in ImagesToFade)
            {
                imageToFade.color = new Color(imageToFade.color.r, imageToFade.color.g, imageToFade.color.b, 1);
            }
            foreach (var text in TextsToFade)
            {
                text.color = new Color(text.color.r, text.color.g, text.color.b, 1);
            }
        }

        IEnumerator FadeText()
        {
            foreach (var text in TextsToFade)
            {
                _fadeColor = new Color(text.color.r, text.color.g, text.color.b, 0);
                text.color = Color.Lerp(text.color, _fadeColor, _fadeSpeed * Time.deltaTime);
                if (Mathf.Abs(text.color.a - _fadeColor.a) <= 0.01)
                {
                    text.color = _fadeColor;
                    _fade = false;

                    if (OnFadeOver != null)
                    {
                        OnFadeOver(this, new EventArgs());
                    }
                }
                yield return null;
            }
        }

        IEnumerator FadeImages()
        {
            foreach (var img in ImagesToFade)
            {
                _fadeColor = new Color(img.color.r, img.color.g, img.color.b, 0);
                img.color = Color.Lerp(img.color, _fadeColor, _fadeSpeed * Time.deltaTime);
                if (Mathf.Abs(img.color.a - _fadeColor.a) <= 0.01)
                {
                    img.color = _fadeColor;
                    _fade = false;

                    if (OnFadeOver != null)
                    {
                        OnFadeOver(this, new EventArgs());
                    }
                }
                yield return null;
            }
        }

        void Update()
        {
            if (_fade)
            {
                StartCoroutine("FadeImages");
                StartCoroutine("FadeText");              
            }
        }
    }
}


