﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SVGImporter;
using TMPro;

namespace Reusable.Scripts
{

    public class MovableObject : MonoBehaviour
    {
        public float MinMoveSpeed;
        public float MaxMoveSpeed;
        public float MinRotateSpeed;
        public float MaxRotateSpeed;
        public bool ScaleDownOnSpotDrop;
        public bool ChangeFontSizeOnSpotDrop;
        public bool DestroyOnSpotDrop;
        public float ZPosition;
        public SVGRenderer Graphics;
        public TextMeshPro TextValue;
        public Drag Dragging;
        public Collider2D ObjectCollider;
        public Animator AnimatorComponent;

        private float _moveSpeed;
        private float _rotateSpeed;

        private Vector3 _targetPosition;
        private float _distanceToTarget;
        private Quaternion _rotation;

        private string objectValue;

        private Action<Collider2D, string, MovableObject> _isItOnRightPosition;
        private Action<int, bool, float> _spawnNewMovableObject;

        private bool _isActive;
        private bool _isSetToSpot;
        private bool _fadeOut;
        private float _rotateAngle;
        private int _forPlayer;
        private int _numOfTurns;
        private Vector3 _spotScale;
        private float _spotFontSize;

        void Awake()
        {
            _distanceToTarget = 10000;
            GetNewTargetPosition();
            _isSetToSpot = false;
        }

        public void Init(string value, Action<Collider2D, string, MovableObject> callbackOnDrop, int forWhichPlayer, SVGAsset graphics, Action<int, bool, float> callbackOnDestroy, bool initialSpawn, float zPosition)
        {
            ZPosition = zPosition;
            Init(value, callbackOnDrop, forWhichPlayer, graphics, callbackOnDestroy, initialSpawn);
        }

        public void Init(string value, Action<Collider2D, string, MovableObject> callbackOnDrop, int forWhichPlayer, SVGAsset graphics, Action<int, bool, float> callbackOnDestroy, bool initialSpawn)
        {
            if (Graphics != null && graphics != null)
            {
                Graphics.vectorGraphics = graphics;
            }

            if (TextValue != null)
            {
                if (value == "6" || value == "9")
                {
                    TextValue.text = "<u>" + value + "</u>";
                }
                else
                {
                    TextValue.text = value;
                }
            }

            objectValue = value;

            _isItOnRightPosition = callbackOnDrop;
            _spawnNewMovableObject = callbackOnDestroy;

            Dragging.SetUpEventsCallbacks(SetAlphaToMax, null, CheckPosition);

            _forPlayer = forWhichPlayer;

            if (initialSpawn)
            {
                transform.position = OutOfSightPosition(UnityEngine.Random.Range(2f, 10f));
            }
            else
            {
                transform.position = OutOfSightPosition(0.7f);
            }

            Vector3 moveDirection = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0)) - gameObject.transform.position;
            transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg);

            _numOfTurns = UnityEngine.Random.Range(3, 6);

            _moveSpeed = UnityEngine.Random.Range(MinMoveSpeed, MaxMoveSpeed);
            _rotateSpeed = UnityEngine.Random.Range(MinRotateSpeed, MaxRotateSpeed);

            _fadeOut = false;
            _isActive = true;
        }

        public void CheckPosition()
        {
            _isItOnRightPosition(ObjectCollider, objectValue, this);
        }

        public int ForWhichPlayer()
        {
            return _forPlayer;
        }

        void Update()
        {
            if (_isActive)
            {
                Rotate();
                Move();

                _distanceToTarget = Vector3.Distance(transform.position, _targetPosition);

                if (_distanceToTarget < 1.5f)
                {
                    GetNewTargetPosition();
                }
            }
            else if (_isSetToSpot)
            {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, 0, _rotateAngle), 180 * Time.deltaTime);
                transform.position = Vector3.Lerp(transform.position, _targetPosition, 5 * Time.deltaTime);

                if (ScaleDownOnSpotDrop)
                {
                    transform.localScale = Vector3.Lerp(transform.localScale, _spotScale, 5 * Time.deltaTime);
                }

                if (ChangeFontSizeOnSpotDrop && TextValue != null)
                {
                    TextValue.fontSize = Mathf.Lerp(TextValue.fontSize, _spotFontSize, 5 * Time.deltaTime);
                }

                if (Vector2.Distance(transform.position, _targetPosition) < 0.002f && Quaternion.Angle(transform.rotation, Quaternion.Euler(0, 0, _rotateAngle)) < 0.002)
                {
                    transform.position = _targetPosition;
                    transform.rotation = Quaternion.Euler(0, 0, _rotateAngle);

                    if (Graphics != null)
                    {
                        Graphics.sortingOrder = 4;
                    }

                    if (TextValue != null)
                    {
                        TextValue.sortingOrder = 4;
                    }

                    if (DestroyOnSpotDrop)
                    {
                        Destroy(gameObject);
                    }
                    else
                    {
                        Destroy(this);
                    }
                }
            }
            else if (_fadeOut)
            {
                if (Graphics != null)
                {
                    Graphics.color = Color.Lerp(Graphics.color, new Color(1, 1, 1, 0), 5 * Time.deltaTime);
                }

                if (TextValue != null)
                {
                    TextValue.color = Color.Lerp(TextValue.color, new Color(1, 1, 1, 0), 5 * Time.deltaTime);
                }
            }
        }

        void Move()
        {
            transform.Translate(Vector3.right * _moveSpeed * Time.deltaTime);
        }

        void Rotate()
        {
            Vector3 moveDirection = _targetPosition - gameObject.transform.position;
            if (moveDirection != Vector3.zero)
            {
                float angle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.AngleAxis(angle, Vector3.forward), _rotateSpeed * Time.deltaTime);
            }
        }

        private void GetNewTargetPosition()
        {
            if (_numOfTurns > 0)
            {
                _targetPosition = Camera.main.ViewportToWorldPoint(new Vector3(UnityEngine.Random.Range(0.1f, 0.9f), UnityEngine.Random.Range(0.1f, 0.9f), Camera.main.nearClipPlane));
                _targetPosition.z = ZPosition;
            }
            else if (_numOfTurns == 0)
            {
                _targetPosition = OutOfSightPosition(2.6f);
            }
            else
            {
                _spawnNewMovableObject(_forPlayer, false, ZPosition);
                Destroy(gameObject);
            }

            _numOfTurns--;
        }

        private Vector3 OutOfSightPosition(float distanceOffset)
        {
            int xOrY = UnityEngine.Random.Range(0, 2);

            float x = 0;
            float y = 0;

            if (xOrY == 0)
            {
                x = UnityEngine.Random.Range(0, 2);
                y = UnityEngine.Random.Range(0f, 1f);
            }
            else
            {
                y = UnityEngine.Random.Range(0, 2);
                x = UnityEngine.Random.Range(0f, 1f);
            }

            Vector3 pos = Camera.main.ViewportToWorldPoint(new Vector3(x, y, Camera.main.nearClipPlane));

            if (xOrY == 0)
            {
                if (x == 0)
                {
                    pos.x -= distanceOffset;
                }
                else
                {
                    pos.x += distanceOffset;
                }
            }
            else
            {
                if (y == 0)
                {
                    pos.y -= distanceOffset;
                }
                else
                {
                    pos.y += distanceOffset;
                }
            }

            pos.z = ZPosition;

            return pos;
        }

        public void SetAlphaToMax()
        {
            _isActive = false;
            transform.position = new Vector3(transform.position.x, transform.position.y, 0);

            if (Graphics != null)
            {
                Graphics.color = Color.white;
                Graphics.sortingOrder = 6;
            }

            if (TextValue != null)
            {
                TextValue.color = Color.white;
                TextValue.sortingOrder = 6;
            }
        }

        public void SetToSpot(Vector3 spotPosition, float rotateToAngle)
        {
            _isActive = false;
            _targetPosition = spotPosition;
            _rotateAngle = rotateToAngle;
            _isSetToSpot = true;
        }

        public void SetToSpot(Vector3 spotPosition, float rotateToAngle, Vector3 newScale)
        {
            _isActive = false;
            _targetPosition = spotPosition;
            _rotateAngle = rotateToAngle;
            _spotScale = newScale;
            _isSetToSpot = true;
        }

        public void SetToSpot(Vector3 spotPosition, float rotateToAngle, Vector3 newScale, float newFontSize)
        {
            _isActive = false;
            _targetPosition = spotPosition;
            _rotateAngle = rotateToAngle;
            _spotScale = newScale;
            _spotFontSize = newFontSize;
            _isSetToSpot = true;
        }

        public void FadeOut()
        {
            if (_isSetToSpot)
            {
                return;
            }

            _isActive = false;
            _fadeOut = true;
            Destroy(gameObject, 1.5f);
        }
    }
}
