﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Reusable.Scripts
{
	[RequireComponent(typeof(AudioSource))]
	public class TTTSoundManager : MonoBehaviour 
	{
		public List<AudioClip> Sounds;
		private AudioSource _audio;
        private List<string> playedSounds = new List<string>();

		void Awake ()
		{
			_audio = GetComponent<AudioSource> ();
		}

		public void Mute (bool muted)
		{
			_audio.mute = muted;
		}

		public void PlaySound(string soundName)
		{
			_audio.clip = Sounds.FirstOrDefault (s => s.name == soundName);
			_audio.Play ();
		}

		public void PlaySound(string soundName, bool loop)
		{
			_audio.loop = loop;
			PlaySound (soundName);
		}

		public void PlaySound(string soundName, bool loop, float volume)
		{
			_audio.volume = volume;
			PlaySound (soundName, loop);
		}

		public void PlayOneShot(string soundName)
		{
			_audio.PlayOneShot (Sounds.FirstOrDefault (s => s.name == soundName));
		}

		public void PlayOneShot(string soundName, float volumeScale)
		{
			_audio.PlayOneShot (Sounds.FirstOrDefault (s => s.name == soundName), volumeScale);
		}

		public void StopSounds()
		{
			_audio.Stop ();
		}

		public bool IsAudioPlaying()
		{
			return _audio.isPlaying;
		}
        public void PlayOneShotIfNotPlayed (string soundName)
        {
            if(!playedSounds.Contains(soundName))
            {
                AudioClip currentSound = Sounds.FirstOrDefault(s => s.name == soundName);
                _audio.PlayOneShot(currentSound);
                playedSounds.Add(soundName);
                StartCoroutine(RemoveSoundAfterSpecificTime(currentSound.length, soundName));
            }
        }
        public IEnumerator RemoveSoundAfterSpecificTime(float soundLength, string soundName)
        {
            yield return new WaitForSeconds(soundLength);
            playedSounds.Remove(soundName);
        }
    }
}
