﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

namespace Reusable.Scripts
{
	[RequireComponent(typeof(Collider2D))]
	public class Drag : MonoBehaviour, IDrag {

        public List<int> TouchIds { get; set; } //not used for now
		private Action _OnDragBegunEvent;
		private Action _OnDragEvent;
		private Action _OnDragEndEvent;

		void Awake()
		{
            TouchIds = new List<int>();
		}

		public void SetUpEventsCallbacks(Action onDragBegunEvent, Action onDragEvent, Action onDragEndEvent)
		{
			_OnDragBegunEvent = onDragBegunEvent;

			_OnDragEvent = onDragEvent;

			_OnDragEndEvent = onDragEndEvent;
		}

		public void RegisterTouch(int touchId, Vector2 newPosition)
		{
			if (TouchIds.Count == 0)
			{
                TouchIds.Add(touchId);
				transform.position = new Vector3 (newPosition.x, newPosition.y, transform.position.z);

				if (_OnDragBegunEvent != null)
				{
					_OnDragBegunEvent ();
				}
			}
		}

		public void MoveTo(int touchId, Vector2 newPosition)
		{
			if (TouchIds.Count > 0)
			{
				transform.position = new Vector3 (newPosition.x, newPosition.y, transform.position.z);

				if (_OnDragEvent != null)
				{
					_OnDragEvent ();
				}
			}
		}

		public void UnregisterTouch(int touchId, Vector2 newPosition)
		{
            TouchIds.Remove(touchId);
			transform.position = new Vector3 (newPosition.x, newPosition.y, transform.position.z);

			if (_OnDragEndEvent != null)
			{
				_OnDragEndEvent ();
			}
		}
	}
}
