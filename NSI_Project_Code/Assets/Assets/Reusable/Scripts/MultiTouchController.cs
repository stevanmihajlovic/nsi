﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Reusable.Scripts
{
    public delegate RaycastHit2D RaycastHitChoose(RaycastHit2D[] raycastHits);
    public class MultiTouchController : MonoBehaviour
    {
        List<IDrag> _touchList = new List<IDrag>();
        LayerMask _touchLayer;
        RaycastHit2D _hit;
        Camera _mainCamera;

        public RaycastHitChoose ChooseBetweenRaycastHits;

        void Awake()
        {
            _touchList = new List<IDrag>();
            _touchLayer = LayerMask.GetMask("TouchLayer");
            _mainCamera = Camera.main;
        }

        void OnEnable()
        {
            _touchList.Clear();
        }

        void Update()
        {
            if (Input.touchCount > 0)
            {
                for (int i = 0; i < Input.touchCount; i++)
                {
                    Touch touch = Input.GetTouch(i);

                    if (touch.phase == TouchPhase.Began)
                    {
                        Ray ray = _mainCamera.ScreenPointToRay(touch.position);

                        //_hit = Physics2D.GetRayIntersection(ray, 1000, _touchLayer);
                        _hit = GetAppropriateRayHit(Physics2D.GetRayIntersectionAll(ray, 1000, _touchLayer));

                        if (_hit.collider != null)
                        {
                            IDrag recipient = _hit.collider.GetComponent<IDrag>();

                            if (recipient != null)
                            {
                                recipient.RegisterTouch(touch.fingerId, _hit.point);

                                if (!_touchList.Contains(recipient)) //One object can have multiple touches on itself
                                    _touchList.Add(recipient);
                            }
                        }
                    }

                    Vector3 touchPosition = _mainCamera.ScreenToWorldPoint(touch.position);

                    if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved)
                    {
                        IDrag tdt = _touchList.FirstOrDefault(x => x.TouchIds.Contains(touch.fingerId));
                        if (tdt != null)
                        {
                            tdt.MoveTo(touch.fingerId, touchPosition);
                        }
                    }

                    if (touch.phase == TouchPhase.Canceled || touch.phase == TouchPhase.Ended)
                    {
                        IDrag tdt = _touchList.FirstOrDefault(x => x.TouchIds.Contains(touch.fingerId));
                        if (tdt != null)
                        {
                            tdt.UnregisterTouch(touch.fingerId, touchPosition);
                            if (tdt.TouchIds.Count == 0)
                                _touchList.Remove(tdt);
                        }
                    }
                }
            }
            else if (Input.GetMouseButtonDown(0))
            {
                Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);

                //_hit = Physics2D.GetRayIntersection(ray, 1000, _touchLayer);
                _hit = GetAppropriateRayHit(Physics2D.GetRayIntersectionAll(ray, 1000, _touchLayer));

                if (_hit.collider != null)
                {
                    IDrag recipient = _hit.collider.GetComponent<IDrag>();

                    if (recipient != null)
                    {
                        recipient.RegisterTouch(0, _hit.point);
                        _touchList.Add(recipient);
                    }
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                IDrag tdt = _touchList.FirstOrDefault(x => x.TouchIds.Contains(0));
                if (tdt != null)
                {
                    tdt.UnregisterTouch(0, _mainCamera.ScreenToWorldPoint(Input.mousePosition));
                    _touchList.Remove(tdt);
                }
            }
            else if (Input.GetMouseButton(0))
            {
                IDrag tdt = _touchList.FirstOrDefault(x => x.TouchIds.Contains(0));
                if (tdt != null)
                {
                    tdt.MoveTo(0, _mainCamera.ScreenToWorldPoint(Input.mousePosition));
                }
            }
        }

        private RaycastHit2D GetAppropriateRayHit(RaycastHit2D[] raycastHits)
        {
            if (raycastHits.Length > 0)
            {
                if (raycastHits.Length == 1)
                {
                    return raycastHits[0];
                }

                if (ChooseBetweenRaycastHits != null)
                {
                    return ChooseBetweenRaycastHits(raycastHits);
                }
            }
            return new RaycastHit2D();
        }

    }
}
