﻿using UnityEngine;

namespace Reusable.Scripts
{
    public class ObjectRotation_Interval : MonoBehaviour
    {
        private Transform _transform;

        public bool Clockwise = true;

        public float RotateDegrees = 90f;
        private float _rotateDegrees;

        public Vector2 RotateTimeRange = new Vector2(3.5f, 4.5f);
        private float _rotateTime;

        public Vector2 PauseTimeRange = new Vector2(5f, 15f);
        private float _pauseTime;

        void Awake()
        {
            _transform = GetComponent<Transform>();

            _rotateDegrees = RotateDegrees;
            if (Clockwise) _rotateDegrees = -RotateDegrees;

            _rotateTime = Random.Range(RotateTimeRange.x, RotateTimeRange.y);
            _pauseTime = Random.Range(PauseTimeRange.x, PauseTimeRange.y);

            float startRotation = (Random.Range(0, 4) * _rotateDegrees);
            transform.localEulerAngles = new Vector3(transform.rotation.x, transform.rotation.y, startRotation);

            float firstStart = _pauseTime / 2;

            if (!IsInvoking("rotate"))
                InvokeRepeating("rotate", firstStart, _rotateTime + _pauseTime);
        }

        private void rotate()
        {
            LeanTween.rotateAround(gameObject, Vector3.forward, _rotateDegrees, _rotateTime)
                .setEase(LeanTweenType.easeInOutQuad);
        }

        private void OnEnable()
        {
            if (!IsInvoking("rotate"))
            {
                float firstStart = _pauseTime / 2;
                InvokeRepeating("rotate", firstStart, _rotateTime + _pauseTime);
            }
        }

        private void OnDisable()
        {
            CancelInvoke("rotate");
        }
    }
}

