﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Reusable.Scripts
{
	public class ResizeSpriteToScreen : MonoBehaviour
	{
		private SpriteRenderer _sr;

		void Awake ()
		{
			_sr = GetComponent<SpriteRenderer>();

			Resize ();
		}

		public void Resize()
		{
			if (_sr == null) return;

			transform.localScale = new Vector3(1,1,1);

			float width = _sr.sprite.bounds.size.x;
			float height = _sr.sprite.bounds.size.y;

			float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
			float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

			float newScaleAxis = Mathf.Max (worldScreenWidth / width, worldScreenHeight / height);

			transform.localScale = new Vector3 (newScaleAxis, newScaleAxis, 1);
		}
	}
}
