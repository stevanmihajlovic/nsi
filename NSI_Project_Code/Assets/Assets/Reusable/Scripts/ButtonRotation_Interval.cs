﻿using UnityEngine;

namespace Reusable.Scripts
{
    public class ButtonRotation_Interval : MonoBehaviour
    {
        private RectTransform _rectTransform;

        public bool Clockwise = true;
        public bool StartRandomRotation = true;

        public float RotateDegrees = 90f;
        private float _rotateDegrees;

        public Vector2 RotateTimeRange = new Vector2(3.5f, 4.5f);
        private float _rotateTime;

        public Vector2 PauseTimeRange = new Vector2(5f, 15f);
        private float _pauseTime;

        void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();

            _rotateDegrees = RotateDegrees;
            if (Clockwise) _rotateDegrees = -RotateDegrees;

            _rotateTime = Random.Range(RotateTimeRange.x, RotateTimeRange.y);
            _pauseTime = Random.Range(PauseTimeRange.x, PauseTimeRange.y);

            if (StartRandomRotation)
            {
                float startRotation = (Random.Range(0, 4) * _rotateDegrees);
                transform.localEulerAngles = new Vector3(transform.rotation.x, transform.rotation.y, startRotation);
            }

            float firstStart = _pauseTime / 9;

            if (!IsInvoking("rotate"))
                InvokeRepeating("rotate", firstStart, _rotateTime + _pauseTime);
        }

        private void rotate()
        {
            LeanTween.rotateAround(_rectTransform, Vector3.forward, _rotateDegrees, _rotateTime)
                .setEase(LeanTweenType.easeInOutQuad);
        }

        private void OnEnable()
        {
            if (!IsInvoking("rotate"))
            {
                float firstStart = _pauseTime / 9;
                InvokeRepeating("rotate", firstStart, _rotateTime + _pauseTime);
            }
        }

        private void OnDisable()
        {
            CancelInvoke("rotate");
        }

        public void SetRotation(bool clockwise)
        {
            CancelInvoke("rotate");
            Clockwise = clockwise;
            if (clockwise)
            {
                _rotateDegrees = -RotateDegrees;
            }
            else
            {
                _rotateDegrees = RotateDegrees;
            }

            float firstStart = _pauseTime / 9;
            InvokeRepeating("rotate", firstStart, _rotateTime + _pauseTime);
        }
    }
}