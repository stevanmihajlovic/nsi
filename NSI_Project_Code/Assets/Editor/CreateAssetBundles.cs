﻿﻿using System.IO;
using UnityEditor;
using UnityEngine;

public class CreateAssetBundles : MonoBehaviour
{
	[MenuItem("Assets/AssetBundles/Build All")]
	public static void BuildAssetBundlesAll()
	{
		BuildAssetBundlesWindows64();
		BuildAssetBundlesWindows32();
		BuildAssetBundlesOSX64();
		BuildAssetBundlesOSX32();
		BuildAssetBundlesOSXUniversal();
		BuildAssetBundlesIOS();
		BuildAssetBundlesAndroid();
	}
	
	[MenuItem("Assets/AssetBundles/Build Windows (64-bit)")]
	public static void BuildAssetBundlesWindows64()
	{
		build("Windows64", BuildTarget.StandaloneWindows64);
	}
	
	[MenuItem("Assets/AssetBundles/Build Windows (32-bit)")]
	public static void BuildAssetBundlesWindows32()
	{
		build("Windows32", BuildTarget.StandaloneWindows64);
	}
	
	[MenuItem("Assets/AssetBundles/Build OSX (64-bit)")]
	public static void BuildAssetBundlesOSX64()
	{
		build("OSX64", BuildTarget.StandaloneOSXIntel64);
	}
	
	[MenuItem("Assets/AssetBundles/Build OSX (32-bit)")]
	public static void BuildAssetBundlesOSX32()
	{
		build("OSX32", BuildTarget.StandaloneOSXIntel);
	}
	
	[MenuItem("Assets/AssetBundles/Build OSX (Universal)")]
	public static void BuildAssetBundlesOSXUniversal()
	{
		build("OSXUniversal", BuildTarget.StandaloneOSX);
	}
	
	[MenuItem("Assets/AssetBundles/Build IOS")]
	public static void BuildAssetBundlesIOS()
	{
		build("IOS", BuildTarget.iOS);
	}
	
	[MenuItem("Assets/AssetBundles/Build Android")]
	public static void BuildAssetBundlesAndroid()
	{
		build("Android", BuildTarget.Android);
	}

	private static void build(string folder, BuildTarget target)
	{
		BuildPipeline.BuildAssetBundles(checkPath(folder), BuildAssetBundleOptions.None, target);
	}

	private static string checkPath(string folder)
	{
		string path = Application.streamingAssetsPath + "/AssetBundles/" + folder;
		Debug.Log("Path: " + path);
		
		if(!Directory.Exists(path))
			Directory.CreateDirectory(path);

		return path;
	}
}